Breadboard Power Supply
=======================

**NB: This project is a work in progress!**


Overview
--------

This small power supply is designed to fit solidly onto a standard
`solderless breadboard`_ (2.125" wide; 0.100" hole pitch), and
provides a user-selectable 3.3 VDC or 5.0 VDC output.

.. _solderless breadboard: https://en.wikipedia.org/wiki/Breadboard#Solderless_breadboard

The power supply has four pairs of power output pin headers mounted to
the bottom of the board, each connected to the same |Vout| and ground
nodes.  The output pin headers are located such that the board will
plug directly into the bus strips on either edge of the breadboard.
Each bus is fed by two pairs of pin headers.

The design and manufacture of this power supply was undertaken as an
exercise in learning schematic capture and PCB layout using Kicad_.
It is very similar to `SparkFun's Breadboard Power Supply`_.  Unlike
the SparkFun board, my design does not include a barrel jack for power
input; does not include a power switch; and does not include a switch
for selecting the output voltage.  As it is intended for use with a 12
VDC bench power supply, all that is needed is a pair of pin headers
for power input.  Another pin header is provided for selection of
output voltage.

.. _Kicad: http://www.kicad-pcb.org/.
.. _SparkFun's Breadboard Power Supply: https://www.sparkfun.com/products/114


Design Details
--------------

* A PolyFuse_ provides over-current protection (in addition to the
  regulator's built-in protection); limiting total current to about
  750 mA.

* Diode D1 serves as reverse-polarity input protection.  A Schottky
  was used for its low forward voltage.

* Input filter capacitor C1 TODO

* Adjustment/feedback resistors R1, R2, R3 and output voltage select
  switch pin header P2  TODO

  Resistors must be precision (tight tolerance), 1% or less.
  Otherwise, the output voltage could differ significantly from what
  is expected.

* Output filter caps C2, C3; and diodes D2, D3 TODO

* LED1 (and its current-limiting resistor R4) serves as a power-on
  indicator.

* `Copper pours`_ on both the front and back side of the PCB serve as
  |Vout| and ground nodes (respectively).  This provides a low
  impedance path for these high current nodes, and simplifies trace
  routing.

* Power out pin headers P4 through P6 are mounted on opposite corners
  of the PCB, on the underside, located so that they each plug into
  the power rails on either side of a common breadboard.  Four pin
  headers were used primarily for board stability.

.. _PolyFuse: https://en.wikipedia.org/wiki/Resettable_fuse
.. _Copper pours: https://en.wikipedia.org/wiki/Copper_pour


.. |Vout| replace:: V\ :sub:`out`
